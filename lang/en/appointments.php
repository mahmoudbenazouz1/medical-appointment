<?php

return [
    'title'         => 'Appointments',
    'returntolist'  => 'Appointment list',
    'name'          => 'Name*',
    'description'   => 'Description*',
    'save'          => 'Save',
    'date'          => 'Date',
    'status'        => 'Status',
    'informations'  => 'Informations',
    'successadd'    => 'The appointment has been correctly added.',
    'successmod'    => 'The appointment has been correctly modified.',
    'create'        => [
        'title'     => 'Add a Appointment',
        'doctor_select'     => 'Choose a doctor',
        'hour_select'     => 'Choose a hour',
    ],
    'edit' => [
        'title'     => 'Edit a Appointment',
        'doctor_select'     => 'Doctor',
        'patient_select'     => 'Patient',
        'confirm_select'     => 'Confirm',
        'hour_select'     => 'Hour',
    ],
    'list' => [
        'title'         => 'Appointment list',
        'confirmdelete' => 'Do you confirm that you want to delete this appointment ?',
        'deletesuccess' => 'The appointment has been correctly deleted',
        'deleteerror'   => 'An error occured when trying to delete the appointment',
    ],
];
