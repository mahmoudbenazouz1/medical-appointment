<?php

use App\Http\Controllers\AppointmentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('boilerplate.login');
});

Route::group([
    // 'prefix'     => config('boilerplate.app.prefix', ''),
    'domain' => config('boilerplate.app.domain', ''),
    'middleware' => ['web', 'boilerplate.locale'],
    'as' => 'boilerplate.',
], function () {
    Route::group(['middleware' => ['boilerplate.auth']], function () {
        // patient routes
        Route::group(['middleware' => ['role:patient']], function () {
            Route::post('appointments', [AppointmentController::class, 'store'])->name('appointments.store');
            Route::get('appointments/create', [AppointmentController::class, 'create'])->name('appointments.create');
        });
        // doctor routes
        Route::group(['middleware' => ['role:doctor']], function () {
            Route::put('appointments/{appointment}/update', [AppointmentController::class, 'update'])->name('appointments.update');
        });
         // patient, doctor routes
        Route::group(['middleware' => ['role:patient|doctor']], function () {
            Route::get('appointments', [AppointmentController::class, 'index'])->name('appointments.index');
            Route::get('appointments/{appointment}/edit', [AppointmentController::class, 'edit'])->name('appointments.edit');
        });
        
    });
});
