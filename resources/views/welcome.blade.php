<!DOCTYPE html>
<html lang='en'>

<head>
  <meta charset='utf-8' />
  <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.12/index.global.min.js'></script>
  <style>
    body {
      font-family: Arial, sans-serif;
      background-color: #f0f0f0;
      margin: 0;
      padding: 0;
    }

    .container {
      max-width: 800px;
      margin: 20px auto;
      padding: 20px;
      background-color: #fff;
      border-radius: 5px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    }

    .input-container {
      display: flex;
      align-items: center;
      margin-bottom: 20px;
    }

    .input-container label {
      margin-right: 10px;
    }

    .input-container select {
      padding: 8px;
      border: 1px solid #ccc;
      border-radius: 5px;
      font-size: 16px;
    }

    #add-event-btn {
      padding: 8px 16px;
      border: none;
      background-color: #007bff;
      color: white;
      border-radius: 5px;
      cursor: pointer;
      font-size: 16px;
    }

    #add-event-btn:hover {
      background-color: #0056b3;
    }

    .fc-event {
      white-space: normal;
      /* Allows the text to wrap to the next line */
    }
  </style>
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
        initialView: 'dayGridMonth',
        initialDate: '2024-05-07',
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        events: [{
            id: '1',
            title: 'All Day Event',
            start: '2024-05-01',
            color: '#000',
            extendedProps: {
              description: 'This is an all day event.'
            }
          },
          {
            id: '2',
            title: 'Conference',
            start: '2024-05-11',
            end: '2024-05-13'
          },
          {
            title: 'Meeting',
            start: '2024-05-12',
          },
          {
            title: 'Lunch',
            start: '2024-05-12T12:00:00'
          },
          {
            title: 'Meeting',
            start: '2024-05-12T14:30:00'
          },
          {
            title: 'Birthday Party',
            start: '2024-05-13T07:00:00'
          },
          {
            title: 'Click for Google',
            url: 'https://google.com/',
            start: '2024-05-28'
          }
        ],

        eventContent: function(arg) {
          let html = `<b>${arg.event.title}</b><br/>`;
          if (arg.event.extendedProps.description) {
            html += `<i>${arg.event.extendedProps.description}</i>`;
          }
          return {
            html: html
          };
        },
        eventClick: function(info) {
          var eventInfo = document.getElementById('eventInfo');
          var modal = document.getElementById('eventModal');
          var span = document.getElementById('close');

          eventInfo.innerHTML = 'Title: ' + info.event.title + '<br>' +
            'Start: ' + info.event.start + '<br>' +
            'End: ' + info.event.end + '<br>' +
            'Description: ' + info.event.extendedProps.description;

          modal.style.display = 'block';

          span.onclick = function() {
            modal.style.display = 'none';
          }

          window.onclick = function(event) {
            if (event.target == modal) {
              modal.style.display = 'none';
            }
          }
        }

      });

      calendar.render();
    });
  </script>
</head>

<body>
  <div class="container">
    <div class="input-container">
      <label for="date-input">Select Date:</label>
      <input type="date" id="date-input">
    </div>
    <div class="input-container">
      <label for="hour-select">Select Hour:</label>
      <select id="hour-select">
        <option value="00">00:00</option>
        <option value="01">01:00</option>
        <option value="02">02:00</option>
        <option value="03">03:00</option>
        <option value="04">04:00</option>
        <option value="05">05:00</option>
        <option value="06">06:00</option>
        <option value="07">07:00</option>
        <option value="08">08:00</option>
        <option value="09">09:00</option>
        <option value="10">10:00</option>
        <option value="11">11:00</option>
        <option value="12">12:00</option>
        <option value="13">13:00</option>
        <option value="14">14:00</option>
        <option value="15">15:00</option>
        <option value="16">16:00</option>
        <option value="17">17:00</option>
        <option value="18">18:00</option>
        <option value="19">19:00</option>
        <option value="20">20:00</option>
        <option value="21">21:00</option>
        <option value="22">22:00</option>
        <option value="23">23:00</option>
      </select>
    </div>
    <button id="add-event-btn">Add Event</button>
    <div id='calendar' style="margin-top: 20px;"></div>
    <div id="eventModal" style="display:none; position:fixed; z-index:1000; left:0; top:0; width:100%; height:100%; overflow:auto; background-color:rgba(0,0,0,0.5);">
      <div style="background-color:#fefefe; margin:15% auto; padding:20px; border:1px solid #888; width:80%;">
        <span id="close" style="color:#aaa; float:right; font-size:28px; font-weight:bold; cursor:pointer;">×</span>
        <p id="eventInfo"></p>
      </div>
    </div>

  </div>
</body>

</html>