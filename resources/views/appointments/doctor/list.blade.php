@extends('boilerplate::layout.index', [
'title' => __('appointments.title'),
'subtitle' => __('appointments.list.title'),
'breadcrumb' => [
__('appointments.list.title') => 'boilerplate.appointments.index'
]
])

@section('content')
@component('boilerplate::card')
<div id='calendar' style="margin-top: 20px;"></div>
@endcomponent
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.12/index.global.min.js'></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var events = <?php echo json_encode($events) ?>;
        var currentDate = new Date();
        var initialDate = currentDate.toISOString().slice(0, 10);

        var calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'timeGridWeek',
            initialDate: initialDate,
            headerToolbar: {
                left: 'prev,next',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
            },
            events: events,
            eventContent: function(arg) {
                let html = `<b>${arg.event.title}</b><br/>`;
                if (arg.event.extendedProps.description) {
                    html += `<i>${arg.event.extendedProps.description}</i>`;
                }
                return {
                    html: html
                };
            },
        });

        calendar.render();
    });
</script>
@endsection

@push('css')
<style>
    .img-circle {
        border: 1px solid #CCC
    }

    body {
        font-family: Arial, sans-serif;
        background-color: #f0f0f0;
        margin: 0;
        padding: 0;
    }

    .container {
        max-width: 800px;
        margin: 20px auto;
        padding: 20px;
        background-color: #fff;
        border-radius: 5px;
        box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    }

    .input-container {
        display: flex;
        align-items: center;
        margin-bottom: 20px;
    }

    .input-container label {
        margin-right: 10px;
    }

    .input-container select {
        padding: 8px;
        border: 1px solid #ccc;
        border-radius: 5px;
        font-size: 16px;
    }

    #add-event-btn {
        padding: 8px 16px;
        border: none;
        background-color: #007bff;
        color: white;
        border-radius: 5px;
        cursor: pointer;
        font-size: 16px;
    }

    #add-event-btn:hover {
        background-color: #0056b3;
    }

    .fc-event {
        white-space: normal;
        /* Allows the text to wrap to the next line */
    }
</style>
@endpush