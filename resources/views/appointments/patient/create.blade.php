@extends('boilerplate::layout.index', [
'title' => __('appointments.title'),
'subtitle' => __('appointments.create.title'),
'breadcrumb' => [
__('appointments.title') => 'boilerplate.appointments.index',
__('appointments.create.title')
]
])

@section('content')
<form method="POST" action="{{ route('boilerplate.appointments.store') }}" autocomplete="off">
    @csrf
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.appointments.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('appointments.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('appointments.save')
                </button>
            </span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'appointments.informations'])
            <div class="row">
                <div class="col-lg-12">
                    @component('boilerplate::select2', ['name' => 'doctor_select', 'label' => 'appointments.create.doctor_select', 'options' => $doctors])@endcomponent
                </div>
                <div class="col-lg-10">
                    <x-boilerplate::datetimepicker name="date" label="Date" />
                </div>
                <div class="col-lg-2">
                    @component('boilerplate::select2', ['name' => 'hour_select', 'label' => 'appointments.create.hour_select', 'options' => $hours])@endcomponent
                </div>
            </div>

            @endcomponent
        </div>
    </div>
</form>

@endsection