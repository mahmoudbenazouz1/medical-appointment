@extends('boilerplate::layout.index', [
'title' => __('appointments.title'),
'subtitle' => __('appointments.edit.title'),
'breadcrumb' => [
__('appointments.title') => 'boilerplate.appointments.index',
__('appointments.edit.title')
]
])

@section('content')
    <div class="row">
        <div class="col-12 pb-3">
            <a href="{{ route("boilerplate.appointments.index") }}" class="btn btn-default" data-toggle="tooltip" title="@lang('appointments.returntolist')">
                <span class="far fa-arrow-alt-circle-left text-muted"></span>
            </a>
            <!-- <span class="btn-group float-right">
                <button type="submit" class="btn btn-primary">
                    @lang('appointments.save')
                </button>
            </span> -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @component('boilerplate::card', ['title' => 'appointments.informations'])
            <div class="row">
                <div class="col-lg-10">
                    @component('boilerplate::input', ['name' => 'doctor', 'label' => 'appointments.edit.doctor_select','value'=> $appointment->doctor->name,'readonly'])@endcomponent
                </div>
                <div class="col-lg-2">
                    @component('boilerplate::input', ['name' => 'status', 'label' => 'appointments.status','value'=> $appointment->status,'readonly'])@endcomponent
                </div>
                <div class="col-lg-10">
                    @component('boilerplate::input', ['name' => 'date', 'label' => 'appointments.date','value'=> \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date)->format('Y-m-d'),'readonly'])@endcomponent
                </div>
                <div class="col-lg-2">
                    @component('boilerplate::input', ['name' => 'hour', 'label' => 'appointments.edit.hour_select','value'=> $appointment->hour->hour,'readonly'])@endcomponent
                </div>
            </div>

            @endcomponent
        </div>
    </div>

@endsection