<?php

namespace App\Models;

use App\Models\Boilerplate\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    const PENDING_STATUS = 'PENDING';
    const PENDING_COLOR = '#FFD700';
    const CONFIRMED_STATUS = 'CONFIRMED';
    const CONFIRMED_COLOR = '#50C878';
    const REJECTED_STATUS = 'REJECTED';
    const REJECTED_COLOR = '#FF5733';
    const CONFIRMED = 'Yes';
    const NOT_CONFIRMED = 'No';

    protected $guarded = [];

    public function doctor()
    {
        return $this->belongsTo(User::class, 'doctor_id', 'id');
    }
    public function patient()
    {
        return $this->belongsTo(User::class, 'patient_id', 'id');
    }

    public function hour()
    {
        return $this->belongsTo(Hour::class, 'hour_id', 'id');

    }
}
