<?php

namespace App\Menu;

use Illuminate\Support\Facades\Auth;
use Sebastienheyd\Boilerplate\Menu\Builder;
use Sebastienheyd\Boilerplate\Menu\MenuItemInterface;

class appointmentMenu implements MenuItemInterface
{
    public function make(Builder $menu)
    {
        if (Auth::user()->hasRole(['patient', 'doctor'])) {
            $item = $menu->add('appointments.title', [
                'icon' => 'square',
                'role' => 'patient,doctor',
                'order' => 1030,
            ]);
            if (Auth::user()->hasRole(['patient'])) {
                $item->add('appointments.create.title', [
                    'route' => 'boilerplate.appointments.create',
                    'role' => 'patient,doctor',
                    'order' => 1002,
                ]);
            }

            $item->add('appointments.list.title', [
                'route' => 'boilerplate.appointments.index',
                'role' => 'patient,doctor',
                'active' => 'boilerplate.appointments.index,boilerplate.appointments.edit',
                'order' => 1003,
            ]);
        }
    }
}
