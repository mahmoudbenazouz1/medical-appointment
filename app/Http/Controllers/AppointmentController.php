<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Boilerplate\User;
use App\Models\Hour;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    public function index()
    {
        $events = [];

        if (Auth::user()->hasRole('patient')) {
            $appointments = Appointment::where('patient_id', Auth::user()->id)->get();
            foreach ($appointments as $appointment) {
                $dateWithHour = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date)
                    ->addHours($appointment->hour->hour)
                    ->format('Y-m-d H:i:s');
                $EndDateWithHour = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date)
                    ->addHours($appointment->hour->hour + 1) // + 1 hour
                    ->format('Y-m-d H:i:s');
                $events[] = [
                    'title' => $appointment->doctor->name,
                    'url' => route('boilerplate.appointments.edit', $appointment),
                    'start' =>  $dateWithHour,
                    'end' =>  $EndDateWithHour,
                    'color' => $appointment->color,
                ];
            }
            return view('appointments.patient.list', [
                'events' => $events
            ]);
        } elseif (Auth::user()->hasRole('doctor')) {
            $appointments = Appointment::where('doctor_id', Auth::user()->id)->get();
            foreach ($appointments as $appointment) {
                $dateWithHour = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date)
                    ->addHours($appointment->hour->hour)
                    ->format('Y-m-d H:i:s');
                $EndDateWithHour = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $appointment->date)
                    ->addHours($appointment->hour->hour + 1) // + 1 hour
                    ->format('Y-m-d H:i:s');
                $events[] = [
                    'title' => $appointment->patient->name,
                    'url' => route('boilerplate.appointments.edit', $appointment),
                    'start' =>  $dateWithHour,
                    'end' =>  $EndDateWithHour,
                    'color' => $appointment->color,
                    'description' => $appointment->comments,

                ];
            }
            return view('appointments.doctor.list', [
                'events' => $events
            ]);
        }
    }
    public function create()
    {
        $doctors = User::whereRoleIs('doctor')
            ->select(DB::raw("CONCAT(first_name, ' ', last_name) AS full_name"), "id")
            ->pluck("full_name", "id")->toArray();

        $hours = Hour::pluck("hour", "id")->toArray();

        return view('appointments.patient.create', [
            'doctors' => $doctors,
            'hours' => $hours
        ]);
    }

    public function edit(Appointment $appointment)
    {
        if (Auth::user()->hasRole('patient')) {
            return view('appointments.patient.edit', [
                'appointment' => $appointment,
            ]);
        } elseif (Auth::user()->hasRole('doctor')) {
            return view('appointments.doctor.edit', [
                'appointment' => $appointment,
            ]);
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'doctor_select' => [
                'required',
                function ($attribute, $value, $fail) {
                    $doctorExists = User::where('id', $value)->first();
                    if (!$doctorExists || !$doctorExists->hasRole('doctor')) {
                        $fail('The selected doctor does not exist or is not a doctor.');
                    }
                },
            ],
            'date' => 'required|date_format:Y-m-d',
            'hour_select' => [
                'required',
                function ($attribute, $value, $fail) {
                    $hourExists = Hour::where('id', $value)->exists();
                    if (!$hourExists) {
                        $fail('The selected hour does not exist.');
                    }

                    // Check if the selected hour is available for the specified doctor on the given date
                    $doctorId = request('doctor_select');
                    $date = request('date');
                    $isHourAvailable = Appointment::where('doctor_id', $doctorId)
                        ->where('hour_id', $value)
                        ->where('date', $date)
                        ->doesntExist();

                    if (!$isHourAvailable) {
                        $fail('The selected hour is not available for the specified doctor on the given date.');
                    }
                },
            ],
        ]);
        $appointment = Appointment::create([
            'doctor_id' => $request->doctor_select,
            'patient_id' => Auth::user()->id,
            'hour_id' => $request->hour_select,
            'status' => Appointment::PENDING_STATUS,
            'color' => Appointment::PENDING_COLOR,
            'date' => $request->date,
        ]);
        return redirect()->route('boilerplate.appointments.edit', $appointment)
            ->with('growl', [__('appointments.successadd'), 'success']);
    }
    public function update(Appointment $appointment, Request $request)
    {
        if ($appointment->doctor_id != Auth::user()->id) {
            abort(403);
        }

        $this->validate($request, [
            'confirm_select' => 'required|in:Yes,No',
            // 'comments' => '',
        ]);

        if ($appointment->status == Appointment::PENDING_STATUS) {
            if ($request->confirm_select == Appointment::CONFIRMED) {
                $appointment->status = Appointment::CONFIRMED_STATUS;
                $appointment->color = Appointment::CONFIRMED_COLOR;
            } else {
                $appointment->status = Appointment::REJECTED_STATUS;
                $appointment->color = Appointment::REJECTED_COLOR;
            }
            $appointment->save();
        }
        return redirect()->route('boilerplate.appointments.edit', $appointment)
            ->with('growl', [__('appointments.successmod'), 'success']);
    }
}
