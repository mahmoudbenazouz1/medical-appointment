<?php

namespace Database\Seeders;

use App\Models\Boilerplate\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // edit backend role to doctor
        $doctor_role = Role::find(2);
        $doctor_role->name = 'doctor';
        $doctor_role->display_name = 'boilerplate::role.doctor.display_name';
        $doctor_role->description = 'boilerplate::role.doctor.description';
        $doctor_role->save();

        // add patient role
        $patient_role = new Role();
        $patient_role->name = 'patient';
        $patient_role->display_name = 'boilerplate::role.patient.display_name';
        $patient_role->description = 'boilerplate::role.patient.description';
        $patient_role->save();

        Role::find(3)->permissions()->sync([1]); // patient role can Access to the back office
    }
}
