<?php

namespace Database\Seeders;

use App\Models\Boilerplate\Role;
use App\Models\Hour;
use Illuminate\Database\Seeder;

class HourSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 8; $i <= 17; $i++) {
            Hour::create([
                'hour'=> $i
            ]);
        }
    }
}
