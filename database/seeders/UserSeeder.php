<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userModel = config('auth.providers.users.model');
        $roleModel = config('laratrust.models.role');

        $admin_user = $userModel::withTrashed()->updateOrCreate(['email' => 'admin@iec-telecom.com'], [
            'active'     => true,
            'first_name' => 'Admin',
            'last_name'  => 'Admin',
            'email'      => 'admin@admin.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $admin = $roleModel::whereName('admin')->first();
        $admin_user->attachRole($admin);

        $doctor_user = $userModel::withTrashed()->updateOrCreate(['email' => 'doctor@doctor.com'], [
            'active'     => true,
            'first_name' => 'Doctor',
            'last_name'  => 'Doctor',
            'email'      => 'doctor@doctor.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $doctor = $roleModel::whereName('doctor')->first();
        $doctor_user->attachRole($doctor);

        $doctor_user = $userModel::withTrashed()->updateOrCreate(['email' => 'doctor2@doctor2.com'], [
            'active'     => true,
            'first_name' => 'Doctor2',
            'last_name'  => 'Doctor2',
            'email'      => 'doctor2@doctor2.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);

        $doctor = $roleModel::whereName('doctor')->first();
        $doctor_user->attachRole($doctor);

        $patient_user = $userModel::withTrashed()->updateOrCreate(['email' => 'patient@patient.com'], [
            'active'     => true,
            'first_name' => 'Patient',
            'last_name'  => 'patient',
            'email'      => 'patient@patient.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);
        $patient = $roleModel::whereName('patient')->first();
        $patient_user->attachRole($patient);

        $patient_user = $userModel::withTrashed()->updateOrCreate(['email' => 'patient2@patient2.com'], [
            'active'     => true,
            'first_name' => 'Patient2',
            'last_name'  => 'patient2',
            'email'      => 'patient2@patient2.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);
        $patient = $roleModel::whereName('patient')->first();
        $patient_user->attachRole($patient);

        $patient_user = $userModel::withTrashed()->updateOrCreate(['email' => 'patient3@patient3.com'], [
            'active'     => true,
            'first_name' => 'Patient3',
            'last_name'  => 'patient3',
            'email'      => 'patient3@patient3.com',
            'password'   => bcrypt('Mahmoud123&'),
            'last_login' => Carbon::now()->toDateTimeString(),
        ]);
        $patient = $roleModel::whereName('patient')->first();
        $patient_user->attachRole($patient);

    }
}
